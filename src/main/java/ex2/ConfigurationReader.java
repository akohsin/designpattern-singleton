package ex2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ConfigurationReader {
    private String path;

    public ConfigurationReader(String path) {
        this.path = path;
    }

    public String[] build() {
        List<String> list = new ArrayList<>();

        try (FileReader readerF = new FileReader(path);
             BufferedReader reader = new BufferedReader(readerF)) {
            String line;
            while (true) {
                line = reader.readLine();
                if (line == null) break;
                list.add(line);
            }

        } catch (FileNotFoundException fnfe) {
            System.out.println("Nie ma takiego pliku.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] tmp = new String[]{};
        return list.toArray(tmp);
    }
}
