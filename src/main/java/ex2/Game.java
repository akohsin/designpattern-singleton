package ex2;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Game {
    private String path = "config.txt";

    private Random random = new Random();

    private int firstNumberRange = Config.getInstance(path).getRangeOfFirstNumber();
    private int secondNumberRange = Config.getInstance(path).getRangeOfSecondNumber();
    private ArrayList<Character> operators = Config.getInstance(path).getOperators();
    private int numberOfRounds = Config.getInstance(path).getNumberOfRounds();

    public void run() {
        Scanner sc = new Scanner(System.in);
        int points = 0;

        for (int i = 0; i < numberOfRounds; i++) {


            int firstNumber = random.nextInt(firstNumberRange);
            int secondNumber = random.nextInt(secondNumberRange);
            char operator = operators.get(random.nextInt(operators.size()));
            int userResult = 0;
            int result = 1;
            System.out.println("pierwsza wylosowana liczba to: " + firstNumber);
            System.out.println("druga wylosowana liczba to: " + secondNumber);
            System.out.println("wylosowany operator to: " + operator);
            System.out.println("podaj odpowiedź: ");
            String line = sc.nextLine();
            if (line.trim().toLowerCase().equals("quit")) {
                break;
            } else {
                userResult = Integer.parseInt(line);
            }
            switch (operator) {
                case 43: {
                    result = secondNumber + firstNumber;
                    break;
                }
                case 45: {
                    result = firstNumber - secondNumber;
                    break;
                }
                case 42: {
                    result = firstNumber * secondNumber;
                    break;
                }
                case 47: {
                    result = firstNumber / secondNumber;
                    break;
                }
            }
            if (result == userResult) {
                points++;
                System.out.println("dobra odpowiedz");
            } else {
                System.out.println("zla odpowiedz");
            }
        }
        System.out.println("twoj wynik to: " + points);
    }
}
