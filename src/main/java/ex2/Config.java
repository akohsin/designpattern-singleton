package ex2;

import java.io.*;
import java.util.ArrayList;

public class Config {
    private static Config instance;
    private static int rangeOfFirstNumber;
    private static int rangeOfSecondNumber;
    private static int numberOfRounds;
    private static ArrayList<Character> operators = new ArrayList<>();

    private Config() {
    }

    public static Config getInstance(String path) {

        if (instance == null) {
            configure(new ConfigurationReader(path).build());
            instance = new Config();
        }
        return instance;
    }

    private static void configure(String[] lines) {
        for (int i = 0; i < lines.length; i++) {
            String tmp[] = lines[i].split("=");

            switch (tmp[0]) {
                case "rangeOfFirstNumber": {
                    rangeOfFirstNumber = Integer.parseInt(tmp[1]);
                    break;
                }
                case "rangeOfSecondNumber": {
                    rangeOfSecondNumber = Integer.parseInt(tmp[1]);
                    break;
                }
                case "numberOfRounds": {
                    numberOfRounds = Integer.parseInt(tmp[1]);
                    break;
                }
                case "operator": {
                    operators.add(tmp[1].charAt(0));
                    break;
                }
            }
        }
    }

    public static int getRangeOfFirstNumber() {
        return rangeOfFirstNumber;
    }

    public static int getRangeOfSecondNumber() {
        return rangeOfSecondNumber;
    }

    public static int getNumberOfRounds() {
        return numberOfRounds;
    }

    public static ArrayList<Character> getOperators() {
        return operators;
    }
}
