package ex1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        HealthDepartment healthDepartment = new HealthDepartment();

        boolean isWorking = true;
        while (isWorking) {
            String line = sc.nextLine().trim().toLowerCase();

            if (line.startsWith("quit")) {
                break;
            } else if (line.startsWith("waitingroom")) {
                healthDepartment.addClientToWaitingRoom();
            } else if (line.startsWith("cash")) {
                healthDepartment.addClientThroughCashRegistry();
            }
        }

    }

}
