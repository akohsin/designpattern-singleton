package ex1;

public class Ticket {
    private int id;
    private String ticketCase;

    public Ticket(int id, String ticketCase) {
        this.id = id;
        this.ticketCase = ticketCase;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", ticketCase='" + ticketCase + '\'' +
                '}';
    }
}
