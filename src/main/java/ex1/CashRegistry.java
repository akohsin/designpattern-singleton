package ex1;

public class CashRegistry {
    public Ticket generateTicket() {
        return new Ticket(TicketGenerator.instance.getCounter(), "Cash Ticket");
    }
}
