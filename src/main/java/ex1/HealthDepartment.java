package ex1;


public class HealthDepartment {

    private CashRegistry cashRegistry;
    private WaitingRoom waitingRoom;

    public HealthDepartment() {
        cashRegistry = new CashRegistry();
        waitingRoom = new WaitingRoom();
    }

    public void addClientThroughCashRegistry() {
        Ticket ticket = cashRegistry.generateTicket();
        System.out.println("Do kasy podchodzi klient, generuje mu ticket: " + ticket);
    }

    public void addClientToWaitingRoom() {
        Ticket ticket = waitingRoom.generate();
        System.out.println("Do poczekalni wchodzi klient, generuje ticket: " + ticket);
    }


}
